
Feature('Google main page');

Before((I) => {
    I.amOnPage('/');
});

Scenario('Test title', (I) => {
    I.seeInTitle('Google');
});
